# coding: utf-8

import time, os, datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup as bs
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException, ElementNotVisibleException

bro = webdriver.Chrome()
bro.set_window_size(600,700)
bro.set_window_position(2400, 110)
bro.get("https://web.whatsapp.com/")
bro.find_element_by_class_name('XSdna').click()

input('PRESS ENTER.')

# while True:
#     try:
#         bro.find_elements_by_xpath("//div[@class='_25Ooe']")
#         break
#     except:
#         pass

log_folder = os.path.abspath('LOG')
if not os.path.exists(log_folder):
    os.makedirs(log_folder)

def log(msg):
    with open(log_folder+'/logfile.txt', 'a') as f:
        f.write(msg+'\n')

def send_msg(msg):
    box = bro.find_element_by_class_name('_2S1VP')
    box.send_keys(msg)
    box.send_keys(Keys.ENTER)

def main():

    for x in bro.find_elements_by_xpath("//div[@class='_25Ooe']"):
        if x.text.find('PyNerds Bot')!=-1:
            x.click()

    lastmsg = ''

    while True:
        html = bro.page_source
        htmlbs = bs(html, 'html.parser')
        
        try:
            chat_msgs = htmlbs.findAll('div', {'class': 'vW7d1'})
            author = chat_msgs[-1].find('div', {'class': '_111ze'})
            meta_data = chat_msgs[-1].find('div', {'class': 'copyable-text'}).get('data-pre-plain-text') #Meta-data da mensagem, não aparece quando envia só imagem.
            last_chat_msg = chat_msgs[-1].find('div', {'class': '_3zb-j'}).text

            if last_chat_msg == '/log':
                send_msg('não enche....')

            msg = meta_data + (last_chat_msg if last_chat_msg else ' ')

            if lastmsg != msg and msg.find('8] ~:')==-1:
                lastmsg=msg
                if last_chat_msg:log(msg)
                print(msg)

        except Exception as e:
            send_msg(str(e))
            print('ERRO: ', e)
            pass
    
        time.sleep(0.3)

if __name__ == '__main__':
    main()