# coding: utf-8

import time, os, datetime, tweepy 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup as bs
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException, ElementNotVisibleException

bro = webdriver.Chrome()
bro.set_window_size(600,700)
bro.set_window_position(2400, 110)
bro.get("https://web.whatsapp.com/")
bro.find_element_by_class_name('XSdna').click()

while True: 
    try: 
        bro.find_element_by_class_name('XSdna') 
        time.sleep(1) 
        pass 
    except: 
        break

admins = ['Rafael Morais', 'Henrique PyNerds', 'Zirou', 'Unknow PyNerds']

log_folder = os.path.abspath('LOG')
if not os.path.exists(log_folder):
    os.makedirs(log_folder)

def log(msg):
    with open(log_folder+'/logfile.txt', 'a') as f:
        f.write(msg+'\n')

def send_msg(msg):
    box = bro.find_element_by_class_name('_2S1VP')
    box.send_keys(msg)
    box.send_keys(Keys.ENTER)

def main():

    for x in bro.find_elements_by_xpath("//div[@class='_25Ooe']"):
        if x.text.find('PyNerds Bot')!=-1:
            x.click()

    lastmsg = ''

    while True:
        html = bro.page_source
        htmlbs = bs(html, 'html.parser')
        
        try:
            chat_msgs = htmlbs.findAll('div', {'class': 'vW7d1'})
            author = chat_msgs[-1].find('div', {'class': '_111ze'})
            meta_data = chat_msgs[-1].find('div', {'class': 'copyable-text'}).get('data-pre-plain-text') #Meta-data da mensagem, não aparece quando envia só imagem.
            last_chat_msg = chat_msgs[-1].find('div', {'class': '_3zb-j'}).text

            _author = ' '.join(meta_data.split()[2:]).strip(':')

            if last_chat_msg == '/log' and _author in admins:
                send_msg('Enviando arquivo de log...')
                try:
                    attach_xpath = '//*[@id="main"]/header/div[3]/div/div[2]/div'
                    send_file_xpath = '//*[@id="app"]/div/div/div[1]/div[2]/span/div/span/div/div/div[2]/span[2]/div/div'
                    attach_type_xpath = '//*[@id="main"]/header/div[3]/div/div[2]/span/div/div/ul/li[3]/input'
                    bro.find_element_by_xpath(attach_xpath).click();time.sleep(1)
                    attach_img_btn = bro.find_element_by_xpath(attach_type_xpath)
                    attach_img_btn.send_keys(os.getcwd() + "/LOG/logfile.txt");time.sleep(1)
                    bro.find_element_by_class_name('_3nfoJ').click()
                    bro.find_element_by_xpath(attach_xpath).click()
                except (NoSuchElementException, ElementNotVisibleException) as e:
                    print(str(e))
                    send_msg((str('...')))
            elif last_chat_msg == '/log' and _author in admins:
                send_msg('Permissão negada.')

            if last_chat_msg.find('/tweet')!=-1 and _author in admins:
                try:
                    consumer_key ="ABC" 
                    consumer_secret ="ABC" 
                    access_token ="ABC" 
                    access_token_secret ="ABC"
                    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
                    auth.set_access_token(access_token, access_token_secret)
                    api = tweepy.API(auth)
                    api.update_status(status=last_chat_msg.split('/tweet ')[1]+' \n\nby: '+_author)
                except Exception as e:
                    print(e)
                send_msg('Tweet!\ntwitter.com/PyNerds')

            elif last_chat_msg.find('/tweet')!=-1 and _author not in admins:
                send_msg('Permissão negada.')

            msg = meta_data + (last_chat_msg if last_chat_msg else ' ')

            if lastmsg != msg and msg.find('8] ~:')==-1:
                lastmsg=msg
                if last_chat_msg:log(msg)
                print(msg)

        except Exception as e:
            send_msg('...')
            print('ERRO: ', e)
            pass
    
        time.sleep(0.3)

if __name__ == '__main__':
    main()