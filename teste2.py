import time, os, datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup as bs

browser = webdriver.Chrome()
#browser = webdriver.Firefox()
browser.get("https://web.whatsapp.com/")

input('PRESS ENTER.')

def send_msg(msg):
    box = browser.find_element_by_class_name('_2S1VP')
    box.send_keys(msg)
    box.send_keys(Keys.ENTER)

def save_msg(msg):
    #verifica se já existe o arquivo de log
    if 'botlog.txt' not in os.listdir():
        with open('botlog.txt', 'w') as f:
            f.write(msg + '\n')

    #Se o arquivo de log já existir, abre como append mode
    with open('botlog.txt', 'a') as f:
        f.write(msg + '\n')

last_chat_msg = ''
lastmsg = ''

while True:

    html = browser.page_source
    htmlbs = bs(html, 'html.parser')
    chat_msgs = htmlbs.findAll('div', {'class': 'vW7d1'}) #Pega todas mensagens visiveis no chat, entrada e saida.
    author = chat_msgs[-1].find('div', {'class': '_111ze'}) #Autor da mensagem
    msg_hour = chat_msgs[-1].find('span', {'class': '_3EFt_'}) #hora da mensagem
    imglink = chat_msgs[-1].find('img', {'class': '_1JVSX'}) #url imagem enviada
    emojis = chat_msgs[-1].findAll('img', {'crossorigin': 'anonymous'})
    msg = ''

    try:
        meta_data = chat_msgs[-1].find('div', {'class': 'copyable-text'}).get('data-pre-plain-text') #Meta-data da mensagem, não aparece quando envia só imagem.
        last_chat_msg = chat_msgs[-1].find('div', {'class': '_3zb-j'}).text
    except:
        pass

    if last_chat_msg == '/log':
        with open('botlog.txt', 'r') as f:
            for ms in f:
                send_msg('LOG: ' + ms)

    try:
        #se tiver meta, adiciona na mensagem, se não, pega hora, data gerada e autor, criando um meta data
        if meta_data:
            msg = meta_data

        else:
            date = datetime.datetime.now().strftime("%d/%m/%y")
            msg = f'[{msg_hour}, {date}] {author}'

        #Se tiver texto, adiciona.
        if last_chat_msg:
            msg = msg + last_chat_msg

        #Se tiver emoji, adiciona.
        if emojis:
            for emoji in emojis:
                msg = msg + ' ' + emoji.get('alt')

        #Se tiver img, adiciona
        if imglink:
            msg = msg + ' ' + imglink.get('src')

        #Se não for uma mensagem de log ou do bot, salva no arquivo de log.
        if lastmsg != msg and 'LOG: ' not in msg and 'BOT: ' not in msg:
            #sendmsg('BOT: Incluindo isso no log... ')
            save_msg(msg)
            lastmsg = msg

    except:
        pass

    try:
        #Salva mensagens de evento, entrada e saída de pessoas do grupo
        msg = chat_msgs[-1].find('span', {'class': '_2ArBI'}).text
        if lastmsg != msg and 'LOG: ' not in msg and 'BOT: ' not in msg:
            #sendmsg('BOT: Incluindo isso no log...')
            save_msg(msg)
            lastmsg = msg

    except:
        pass
